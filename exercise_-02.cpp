/*
Nama		: David Aditya Susanto
NPM			: 140810190067
Deskripsi	: Program Delete After Doubly Linked List
Tanggal		: 28 Maret 2020
*/
#include<iostream>
#include <limits>
#include <iomanip>
#include <windows.h>
using namespace std;

struct Node{
		
		int data;
		Node *next;
		Node *prev;;
};

typedef Node *pointer;
typedef pointer list;

void createList(list &head){
	head = NULL;
}


void createNode(pointer &p_new);

void insertFirst(list &head, pointer p_new);

void declareNode(list& head){ //fungsi untuk membuat data awal
	pointer p1, p2, p3, p4, p5;
	p1=new Node;
	p2=new Node;
	p3=new Node;
	p4=new Node;
	p5=new Node;

	head=p1;
	p1->data = 2;
	p1->prev = NULL;
	p1->next = p2;

	p2->data = 7;
	p2->prev = p1;
	p2->next = p3;

	p3->data = 3;
	p3->prev = p2;
	p3->next = p4;

	p4->data = 1;
	p4->prev = p3;
	p4->next = p5;

	p5->data = 8;
	p5->prev = p4;
	p5->next = NULL;
}

void traverse(list head);
void search(list head,pointer &p_search, int key);
void deleteFirst(list &head, pointer &p_del);
void deleteAfter(list &head,pointer &p_del, int key);
void check(int &a);
void inti();
void keluar();



int main(){
	int a, menu;
	int key;
		list List;
		pointer p,del,search;
		createList(List);
		while(1){
		inti();
				cout << " Klik Nomor Menu \n"; cin >> menu; check(menu);
				switch(menu){
				case 1:
				createNode(p);
				insertFirst(List,p);
				traverse(List);

				case 2:
				createNode(p);
				declareNode(List);
				deleteFirst(List,del);
				traverse(List);

				case 3:
				createNode(p);
				declareNode(List);
				deleteAfter(List,search, key);
				traverse(List);

				case 4:
				keluar();

					default:
					system("cls");
					break;
}
}
}

void inti(){
		cout << "---------------Program doubly Linked List ------------------\n";
		cout << "1.Insert List \n";
		cout << "2.Delete First \n";
		cout << "3.Delete After \n";
		cout << "4.Keluar \n";
}

void createNode(pointer &p_new){
		
		p_new = new Node;
		cin >> p_new-> data;
		p_new->next = NULL;
		p_new->prev = NULL;
}

void insertFirst(list& head, pointer p_new){
		
		if (head == NULL){

				head = p_new;
		}
		else{
			p_new->next = head;
			head->prev = p_new;
			head = p_new;
		}
}

void traverse(list head){
		
		pointer temp;
		if(head == NULL){
			cout << "empty list " << endl;
		}

		else{

			cout << temp->data << endl;
			temp = temp-> next;
		}
}

void deleteFirst (list &head, pointer &p_del){
		
		if (head == NULL){
			p_del = NULL;
		}

		else if(head->next == NULL){
				p_del = head;
				head = NULL;
		}
		else{
			p_del = head;
			head = head->next;
			head->prev = NULL;
			p_del-> next = NULL;
		}

}
void search(list head,pointer &p_search,int key){
		p_search = head;
		while (p_search!= NULL){
			if(p_search->data == key){
				break;
			}
			else{
				p_search = p_search->next;
			}
		}
}

void deleteAfter(list &head,pointer &p_del, int key){
	search(head, p_del,key);
	pointer after;
	after = p_del->next;
	if(after == NULL){
		cout << "Node tidak ditemukan \n";
	}
	else {
		if(after->next == NULL){
			after->prev->next=NULL;
			after->prev = NULL;
		}
		else{
				after->prev->next = after->next;
				after->next->prev = after->prev;
				after->next = NULL;
				after->prev = NULL;
		}
	}
}

void check(int& a){
	while(1){
		if(cin.fail()){
			cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(),'\n');
		cout<<"Tolong masukkan angka! ";cin>>a;
	}
		if(!cin.fail())
	break;
	}
}

void keluar(){
	cout << "\n---Terima kasih Telah Menggunakan Program Ini---";
	Sleep(1200);
	system("cls");
	exit(0);
}

